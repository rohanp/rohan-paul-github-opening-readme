### Hi there 👋

- 🔭 **Currently transitioning my career into Machine-Learning, Data-Science & Deep-Learning. Before this, over the previous two years worked as a Front-End Engineer (React, React-Native and Angular stack) in Bangalore and Hyderabad (India) startup space in Fintech and Shipping-tech domain**

- 📫 Connect with me on these **Social Media** platforms.

<a href="https://www.linkedin.com/in/rohan-paul-b27285129/">
    <img align="left" alt="Rohan Paul | Linkedin" width="24px" src="https://github.com/rohan-paul/rohan-paul/blob/master/assets/Linkedin.svg" />
  </a>

  <a href="https://medium.com/@paulrohan">
    <img align="left" alt="Rohan Paul | Medium" width="24px" src="https://github.com/rohan-paul/rohan-paul/blob/master/assets/medium.svg" />
  </a>

   <a href="https://twitter.com/paulr_rohan">
    <img align="left" alt="Rohan Paul | Twitter" width="24px" src="https://github.com/rohan-paul/rohan-paul/blob/master/assets/twitter.svg" />
  </a>

  <a href="https://www.youtube.com/channel/UC0_a8SNpTFkmVv5SLMs1CIA">
    <img align="left" alt="Rohan Paul | Youtube" width="24px" src="https://github.com/rohan-paul/rohan-paul/blob/master/assets/youtube.svg" />
  </a>

   <a href="https://www.kaggle.com/paulrohan2020">
    <img align="left" alt="Rohan Paul | Kaggle" width="50px" height="30px" src="https://github.com/rohan-paul/rohan-paul/blob/master/assets/kaggle.png" />
</a>.

## More about me

- ⚡ **Self-taught Developer and super unusual Career-changer from International Banking to Software Engineering**. Currently living and working in Bangalore, India.

- ⚡ **Ex Deutsche Bank and Ex HSBC Financial Analyst (India and Australia)**.

- ⚡ **About my career transformation** - After working for 10 years in International Banking (across India, Australia and for a short while in NewYork) transitioned to Software Engineering, for my love of coding and solving problems.

- 🌱 Currently working through Mathematics and Statistics fundamental techniques (e.g. Linear and Multi-variate Algebra) and other concepts in the context of implementing them in Machine Learning and Data Science ...

* 🔭 **Currently working on Tensorflow, Pandas, Matplotlib and couple of other Machine-Learning and Data-Science libraries and tools.**

### On my blog

- [Concepts of Differential Calculus for understanding derivation of Gradient Descent in Linear Regresstion](https://medium.com/@paulrohan/concepts-of-differential-calculus-for-understanding-derivation-of-gradient-descent-in-linear-de59a17496a3)
- [Installing Tensorflow with CUDA & cuDNN GPU support on Ubuntu 20.04 and charge through your Linear Algebra calculations](https://medium.com/@paulrohan/installing-tensorflow-with-cuda-cudnn-gpu-support-on-ubuntu-20-04-f6f67745750a)

- [Python — List vs Tuple vs Dictionary](https://medium.com/@paulrohan/python-list-vs-tuple-vs-dictionary-4a48655c7934)

* [Implementing Heap Algorithm of Permutation in JavaScript](https://medium.com/@paulrohan/implemetning-heap-algorithm-to-find-permutation-of-a-set-of-numbers-in-javascript-d6b6ef8ee0e)

* [Deploying a React-Node-MongoDB app to Google Cloud Platform for Free](https://medium.com/@paulrohan/deploying-a-react-node-mongodb-app-to-google-cloud-platforms-google-app-engine-1ba680447d59)

* [Angular — avoid subscribe() method for observables](https://medium.com/@paulrohan/angular-avoiding-subscribe-method-by-replacing-it-with-an-asynpipe-when-possible-a92c20793357)

* [Converting JavaScript callbacks to Promise and Async-Await & replacing Async-waterfall method with a Promise](https://medium.com/javascript-in-plain-english/converting-javascript-callbacks-to-promise-and-async-await-replacing-async-waterfall-method-with-3c8b7487e0b9)

* [Sending verification OTP to user-email with mailgun in a React, Node and Mongo App](https://medium.com/@paulrohan/sending-verification-otp-to-user-email-with-mailgun-in-a-react-node-and-mongo-app-56ba7e4ac29)

* [How bcryptjs works](https://medium.com/javascript-in-plain-english/how-bcryptjs-works-90ef4cb85bf4)

<!-- blog starts -->

<!--
**rohan-paul/rohan-paul** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

<img src="https://i.imgur.com/DI20MKk.png" alt="drawing" width="600"/>

![Rohan's github stats](https://github-readme-stats.vercel.app/api?username=rohan-paul&count_private=true&show_icons=true&theme=radical)

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=rohan-paul)](https://github.com/rohan-paul/github-readme-stats)
